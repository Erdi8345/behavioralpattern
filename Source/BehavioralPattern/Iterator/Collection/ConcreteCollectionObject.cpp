#include "ConcreteCollectionObject.h"
#include "BehavioralPattern/Iterator/Iterators/ConcreteIterator.h"

UIterator* UConcreteCollectionObject::CreateFriendsIterator()
{
	auto Iterator = NewObject<UConcreteIterator>(this, UConcreteIterator::StaticClass());

	if(Iterator)
		Iterator->SetCollection(this);

	return Iterator;
}